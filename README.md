#  Cooperativa 5B                        ![Sello UNL](https://upload.wikimedia.org/wikipedia/commons/d/df/UNL3.png) 

**Cooperativa 5B**, es un proyecto desarrollado para la clase de *Programación Avanzada*, de la Universidad Nacional de Loja. Su propósito es, el de permitir el acceso a la base de datos por medio de un ***Web Service***, desarrollado en **DJANGO**.  

El Repositorio se divide en 4 ramas:
- Master
- Small
- APIView
- GenericAPIVIew

1. La rama master tiene una forma alternativa del diseño de la base de datos.
2. La rama small posee el diseño de la base de datos que se esta usando en el proyecto
3. La rama APIView contiene el proyecto usando las views de la clase APIView.
> Ninguna de estas ramas posee Interfaces de Usuario, son solo ***pruebas***.
5. La rama GenericAPIView contiene el proyecto **Completo**, del cual, su ejecución se especifica a continuación.
> El cliente fue desarrollado en NodeJS.
> El sistema fue escrito en un ordenador con la distribución GNU/Linux Debian 9.6.

## Pre -Requisitos

> De preferencia Ejecutar los servicios desde GNU/Linux.

1. ***git***
2. ***python 2.7 o superior***

### virtualenv

#### Instalación

```bash
 $ pip install virtualenv
```

#### Crear un entorno virtual para el proyecto

```bash
 $ virtualenv cooperativaWS
```
cargar los módulos necesarios:

```bash
 $ cd cooperativaWS
 $ source bin/activate
```
> Para desactivar escribir en la terminal el comando *`deactivate`*.

```bash
 (cooperativaWS)$ pip install django
 (cooperativaWS)$ pip install djangorestframework
 (cooperativaWS)$ deactivate
```

### Instalar Nodejs 

> Si no tiene instalado *sudo*, se puede usar el perfil de superusuario (su).

```bash
 $ sudo apt-get update
 $ sudo apt-get install build-essential libssl-dev
```
> Buscar la ultima versión de NVM en [GitHub NVM](https://github.com/creationix/nvm)

```bash
 $ curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh -o install_nvm.sh
```
Verificar que el archivo se descargo con éxito, E inspeccionar el script de instalación con `nano`.

```bash
 $ nano install_nvm.sh
```

Ejecutar el script con `bash`:

```bash
 $ bash install_nvm.sh
 ```
 
 > Si se uso comando del repositorio de GitHub los dos comandos anteriores son **innecesarios**.
 
 ```bash
 $ source ~/.profile
 $ nvm ls-remote
```
Ver cual es la última versión de NodeJS e instalarla. Instalar el modulo `express`.

```bash
 $ nvm install 11.6.0
 $ nvm use 11.6.0
 $ nvm alias default 11.6.0
 $ nvm use default
 $ node -v
 $ npm install -g express
 $ npm link express
```

## Instalación

***Clonar el repositorio dentro de la carpeta del entorno virtual (cooperativaWS)***
```bash
 $ cd cooperativaWS
 $ git clone https://gitlab.com/JhonCarrion/cooperativa.git
 $ source bin/activate
 (cooperativaWS)$ cd cooperativa
 (cooperativaWS)$ python manage.py makemigrations
 (cooperativaWS)$ python manage.py migrate
 (cooperativaWS)$ python manage.py runserver
```
Con esto se descargo el proyecto y se levantó el servicio REST de DJango. Ahora se debe levantar el servicio de NodeJS.

>**Se debe crear un usuario desde la [página de administración de DJango](http://localhost:8000/admin)**; un usuario de cooperativa, no de DJango.

**Abrir otra terminal y acceder a la carpeta cooperativaWS**

```bash
 $ cd cooperativaWS
 $ cd cooperativa
 $ cd static
 $ npm install
 $ npm install -g nodemon
 $ nodemon
```
> *nodemon* permite actualizar el servicio, en tiempo real, cada vez que se realiza un cambio. 

***AHORA ABRIR EL NAVEGADOR Y ACCEDER A [http://localhost:3000](http://localhost:3000)***
## Licencia
Esta obra está bajo una [licencia de Creative Commons Reconocimiento 4.0 Internacional](http://creativecommons.org/licenses/by/4.0/).